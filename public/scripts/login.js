import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyC-aIVlcx_JutFB0dLLwWCskynddFR9SEI",
    authDomain: "tpmoroni-ab269.firebaseapp.com",
    databaseURL: "https://tpmoroni-ab269-default-rtdb.firebaseio.com",
    projectId: "tpmoroni-ab269",
    storageBucket: "tpmoroni-ab269.appspot.com",
    messagingSenderId: "170765945889",
    appId: "1:170765945889:web:ff48da51d6eabf35e2a760"
  };
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);

  const auth = getAuth(app)

const database = getDatabase(app)

let correoRefLog = document.getElementById("Email-Login");
let passRefLog = document.getElementById("Password-Login"); //referencias al html
let buttonRefLog = document.getElementById("Boton-Login");

buttonRefLog.addEventListener("click", LogIn); //cuando haga click en el boton log in se ejecutara la funcion LogIn


function LogIn (){ //funcion para loguearse

    if((correoRefLog.value != '') && (passRefLog.value != '')){

        signInWithEmailAndPassword(auth, correoRefLog.value, passRefLog.value)
        .then((userCredential) => {
            const user = userCredential.user;
            window.location.href = "./pages/index.html"; //si el logueo es correcto sos reedireccionado a la pagina del sensor
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message; 
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage); //en caso de error en la consola se mostrara el numero del error y tambien el mensaje de error del mismo
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos."); //si no completas los campos se ejecuta esta alerta
    }    

}
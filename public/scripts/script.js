const firebaseConfig = {
    apiKey: "AIzaSyC-aIVlcx_JutFB0dLLwWCskynddFR9SEI",
    authDomain: "tpmoroni-ab269.firebaseapp.com",
    databaseURL: "https://tpmoroni-ab269-default-rtdb.firebaseio.com",
    projectId: "tpmoroni-ab269",
    storageBucket: "tpmoroni-ab269.appspot.com",
    messagingSenderId: "170765945889",
    appId: "1:170765945889:web:ff48da51d6eabf35e2a760"
  };
  // inicio de firebase
  firebase.initializeApp(firebaseConfig);

$(document).ready(function(){
    var database = firebase.database();
	var prenderalarma;

	database.ref().on("value", function(snap){
		prenderalarma = snap.val().prenderalarma; //foto del valor de prenderalarma
		if(prenderalarma == "1"){    //checkeo de firebase
			
			document.getElementById("unact").style.display = "none";
			document.getElementById("act").style.display = "block";
		} else {
			
			document.getElementById("unact").style.display = "block";
			document.getElementById("act").style.display = "none";
		}
	});

    $(".toggle-btn").click(function(){
		var firebaseRef = firebase.database().ref().child("prenderalarma");

		if(prenderalarma == "1"){    // publicar en firebase el valor de prender alarma
			firebaseRef.set("0");
			prenderalarma = "0";
		} else {
			firebaseRef.set("1");
			prenderalarma = "1";
		}
	})
});
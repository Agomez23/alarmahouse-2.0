import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyC-aIVlcx_JutFB0dLLwWCskynddFR9SEI",
    authDomain: "tpmoroni-ab269.firebaseapp.com",
    databaseURL: "https://tpmoroni-ab269-default-rtdb.firebaseio.com",
    projectId: "tpmoroni-ab269",
    storageBucket: "tpmoroni-ab269.appspot.com",
    messagingSenderId: "170765945889",
    appId: "1:170765945889:web:ff48da51d6eabf35e2a760"
  };
  // inicio de firebase
  const app = initializeApp(firebaseConfig);

  const auth = getAuth(app)

const database = getDatabase(app)

let correoRefReg = document.getElementById("Email-Registrarse");
let passRefReg = document.getElementById("Password-Registrarse"); //referencias al html
let buttonRefReg = document.getElementById("Boton-Registrarse");


buttonRefReg.addEventListener("click", RegistroUser); //cuando haga click en boton registrarse ejecutar registro user

function RegistroUser(){ //funcion que se encarga de crear usuartios y autenticarlos con su debida contraseña en firebase

    if((correoRefReg.value != '') && (passRefReg.value != '')){

    createUserWithEmailAndPassword(auth, correoRefReg.value, passRefReg.value)
  .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Creación de usuario.");
            let correo = correoRefReg.value;


            set(ref(database, "mailUsuarios/" + user.uid), {
                Email: correo,
            }).then(() => {
                window.location.href = "./pages/index.html"; //esta parte escribe en la base de datos los mails de los usuarios que se van registrando
            })
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage); //en el caso de que algo pase mal la consola dira el numero del error y el porque del mismo
  });
}
else{
            alert("Revisar que los campos de usuario y contraseña esten completos."); //si hago click y no hay nada en los input saltara esta alerta
        }  
}